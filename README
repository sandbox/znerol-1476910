Secrets
=======

This module offers a generic approach to store password-like secrets into the
database. Just like a password, a secret does not get stored in plain text but
rather a salted hash thereof. Therefore the clear text of a secret cannot be
retrieved from the database. But given a clear text string it is possible to
verify if it matches the hash stored in the database.

API
---
The secret module does not expose any user interface but only two methods and
one hook which can be used by third party modules.

* secrets_put_secret($password, $expire = 0x7FFFFFFF)
  Given a password in plain text, create a new record in the database which is
  valid until the given expiry date (unix timestamp). Returns the sid (secret
  id) on success.
* secret_check_secret($sid, $password)
  Verify if the given plain text password matches the secret with the given
  sid. Returns true or false.
* hook_secrets_delete_multiple(array $sids)
  Invoked whenever one or more secrets are going to be deleted (after hitting
  their expiry date).


Alternative Hashing Methods
---------------------------
By default the mechanism for Drupal 7 passwords is reused but alternative
methods are supported by means of customizable settings. An implementation
leveraging the original phpass class is included in the module. Set the
variable "secrets_password_inc_name" to "secrets.phpass.password" and verify on
the status report page that the changes took effect.

The original phpass library needs to be placed into sites/all/libraries in
order to be found by the secrets module.

Note that the drupal hashes are not strictly compatible with the original
phpass hashes. Therefore you may not switch forth and back between them.
