<?php
/**
 * @file
 * Default implementations of secrets_hash_password and secrets_check_password
 * relying on the standard drupal password.inc.
 */

/**
 * Return true if the original password.inc implementation is used throughout
 * this drupal installation. If a site employs another password scheme, the
 * site must employ its own versions of secrets_hash_password and
 * secrets_check_password.
 */
function secrets_password_inc_check() {
  return (variable_get('password_inc', 'includes/password.inc') == 'includes/password.inc');
}


/**
 * Called from within secrets_requirements, return an array suitable for
 * returning from hook_requirements.
 */
function secrets_password_inc_requirements($phase) {
  $t = get_t();

  $ok = secrets_password_inc_check();

  if ($ok) {
    $requirements['secrets']['value'] = $t('Secrets module is using default Drupal 7 password hashing method.');
  }
  else {
    $requirements['secrets'] = array(
      'description' => $t('It appears that you are not using the default Drupal 7 password hashing method. Therefore you need to replace the methods <code>secrets_hash_password</code> and <code>secrets_check_password</code> with alternative versions which do not reuse methods from default <code>password.inc</code> file. You can point the secrets module to a file containing those functions by setting the variables <code>secrets_password_inc_type</code>, <code>secrets_password_inc_module</code> and <code>secrets_password_inc_name</code>, which are used as the parameters to <code>module_load_include</code>.'),
      'severity' => REQUIREMENT_ERROR,
      'value' => $t('You are using an alternative password.inc file'),
    );
  }

  return $requirements;
}


/**
 * Check whether a given clear text-string produces the same hash like the one
 * we've stored in the database.
 */
function secrets_hash_password($password, $count_log2 = 0) {
  if (secrets_password_inc_check()) {
    require_once DRUPAL_ROOT . '/includes/password.inc';

    if (empty($count_log2)) {
      // Use the standard iteration count.
      $count_log2 = variable_get('password_count_log2', DRUPAL_HASH_COUNT);
    }
    return _password_crypt('sha512', $password, _password_generate_salt($count_log2));
  }
}


/**
 * Copied and adapted from drupal includes/password.inc
 */
function secrets_check_password($stored_hash, $password) {
  if (secrets_password_inc_check()) {
    require_once DRUPAL_ROOT . '/includes/password.inc';

    $type = substr($stored_hash, 0, 3);
    switch ($type) {
    case '$S$':
      // A normal Drupal 7 password using sha512.
      $hash = _password_crypt('sha512', $password, $stored_hash);
      break;
    default:
      return FALSE;
    }
    return ($hash && $stored_hash == $hash);
  }
}
