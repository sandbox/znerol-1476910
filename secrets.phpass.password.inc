<?php
/**
 * @file
 * Implementations of secrets_hash_password and secrets_check_password
 * relying on the unmodified version of phpass.
 *
 * @see http://www.openwall.com/phpass/
 */

define('SECRETS_PHPASS_PATH', DRUPAL_ROOT . '/sites/all/libraries/phpass-0.3/PasswordHash.php');
define('SECRETS_PHPASS_HASH_COUNT', 8);

/**
 * Return true if the original password.inc implementation is used throughout
 * this drupal installation. If a site employs another password scheme, the
 * site must employ its own versions of secrets_hash_password and
 * secrets_check_password.
 */
function secrets_password_inc_check() {
  return file_exists(SECRETS_PHPASS_PATH);
}


/**
 * Called from within secrets_requirements, return an array suitable for
 * returning from hook_requirements.
 */
function secrets_password_inc_requirements($phase) {
  $t = get_t();

  $ok = secrets_password_inc_check();

  if ($ok) {
    $requirements['secrets']['value'] = $t('Secrets module is using the original phpass password hashing method.');
  }
  else {
    $requirements['secrets'] = array(
      'description' => $t('Secrets module is configured to use phpass password hashing method but the required library is not present in your installation. Please download the phpass library from <a href="!link">!link</a> and install the PasswordHash class into !path.',
        array('!link' => 'http://www.openwall.com/phpass/', '!path' => SECRETS_PHPASS_PATH)),
      'severity' => REQUIREMENT_ERROR,
      'value' => $t('Library missing: phpass'),
    );
  }

  return $requirements;
}


function _secrets_password_inc_phpass_create_hasher($count_log2 = 0) {
  require_once SECRETS_PHPASS_PATH;

  if (empty($count_log2)) {
    // Use the standard iteration count.
    $count_log2 = variable_get('secrets_phpass_count_log2', SECRETS_PHPASS_HASH_COUNT);
  }
  $portable_hashes = variable_get('secrets_phpass_portable_hashes', FALSE);

  return new PasswordHash($count_log2, $portable_hashes);
}


/**
 * Compute the hash for a given clear-text string and return it.
 */
function secrets_hash_password($password, $count_log2 = 0) {
  if (secrets_password_inc_check()) {
    $hasher = _secrets_password_inc_phpass_create_hasher($count_log2);
    return $hasher->HashPassword($password);
  }
}


/**
 * Check whether a given clear text-string produces the same hash like the one
 * we've stored in the database.
 */
function secrets_check_password($stored_hash, $password) {
  if (secrets_password_inc_check()) {
    $hasher = _secrets_password_inc_phpass_create_hasher();
    return $hasher->CheckPassword($password, $stored_hash);
  }
}
